package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class DeletePostPage extends BasePage {

    public DeletePostPage() {
        super("posts.url");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public void deletePost() {
        //navigateToPage();


        actions.waitFor(1000);
        actions.clickElementAfterWait("postComment.Button");

        actions.waitFor(1000);
        actions.clickElementAfterWait("exploreThisPost.Button");
        actions.waitFor(1000);
        actions.clickElementAfterWait("postDelete.Button");
        actions.waitFor(1000);
        Select selectBox = new Select(driver.findElement(By.id("StringListId")));
        selectBox.selectByVisibleText("Delete post");
        actions.waitFor(1000);
        actions.clickElementAfterWait("savePost.Button");
    }
    public void assertDeletePost() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("deletePost.Test");
    }
}

