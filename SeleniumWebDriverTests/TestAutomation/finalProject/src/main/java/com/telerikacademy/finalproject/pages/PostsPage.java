package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class PostsPage extends BasePage {
    public PostsPage() {
        super("posts.url");
    }


    public void sortPostsByComments() {
        actions.clickElement("comment.Button");
    }

    public void sortPostsByLikes() {
        actions.clickElement("likePost.Button");
    }

    public void createPrivatePost() {
        actions.waitFor(1000);
        actions.clickElementAfterWait("new.Post");
        actions.waitFor(1000);
        actions.typeValueInField("Selenium Private Post Test", "message.Field");
        actions.waitFor(1000);
        actions.clickElementAfterWait("savePost.Button");
    }

    public void createPublicPost() {
        actions.waitFor(1000);
        actions.clickElementAfterWait("new.Post");
        Select selectBox = new Select(driver.findElement(By.id("StringListId")));
        selectBox.selectByVisibleText("Public post");
        actions.waitFor(1000);
        actions.typeValueInField("Selenium Public Post Test", "message.Field");
        actions.waitFor(1000);
        actions.clickElementAfterWait("savePost.Button");
    }
    public void editPost() {
        actions.waitFor(1000);
        actions.clickElementAfterWait("exploreThisPost.Button");
        actions.waitFor(1000);
        actions.clickElementAfterWait("editPost.Button");
        actions.waitFor(1000);
        actions.typeInFieldAfterWait("Selenium Edit Post Test", "message.Field");
        actions.waitFor(1000);
        actions.clickElementAfterWait("savePost.Button");
    }
    public void deletePost() {
        actions.waitFor(1000);
        actions.clickElementAfterWait("exploreThisPost.Button");
        actions.waitFor(1000);
        actions.clickElementAfterWait("postDelete.Button");
        actions.waitFor(1000);
        Select selectBox = new Select(driver.findElement(By.id("StringListId")));
        selectBox.selectByVisibleText("Delete post");
        actions.waitFor(1000);
        actions.clickElementAfterWait("savePost.Button");
    }

    public void likePost() {
        //navigateToPage();
        actions.waitFor(1000);
        actions.clickElementAfterWait("likePost.Button");
    }
    public void dislikePost() {
        //navigateToPage();
        actions.waitFor(1000);
        actions.clickElementAfterWait("dislikePost.Button");
    }

    public void commentPost() {
        actions.waitFor(1000);
        actions.clickElementAfterWait("exploreThisPost.Button");
        actions.waitFor(1000);
        actions.typeValueInField("Selenium Comment Test", "message.Field");
        actions.waitFor(1000);
        actions.clickElementAfterWait("savePost.Button");
    }
    public void assertPrivatePostExists() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("privatePost.Test");
    }

    public void assertPublicPostExists() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("publicPost.Test");
    }

    public void assertEditPost() {
        navigateToPage();
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("editPost.Test");
    }

    public void assertDeletePost() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("deletePost.Test");
    }

    public void assertLikePost() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("dislikePost.Button");
    }

    public void assertDislikePost() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("likePost.Button");
    }

    public void assertCommentPost() {
        navigateToPage();
        actions.clickElementAfterWait("exploreThisPost.Button");
        actions.waitFor(1000);
        actions.clickElementAfterWait("showComments.Button");
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("commentBody");
    }
}

