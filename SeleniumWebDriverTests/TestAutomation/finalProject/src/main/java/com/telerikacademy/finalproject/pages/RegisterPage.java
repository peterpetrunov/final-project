package com.telerikacademy.finalproject.pages;


public class RegisterPage extends BasePage {
    public RegisterPage() {
        super("register.url");
    }

    public void registerNewUser() {
        actions.waitFor(1000);
        actions.typeValueInField("Newtestuser", "registerUsername");
        actions.typeValueInField("NewTestUser8504@gmail.com", "registerEmail");
        actions.typeValueInField("password", "registerPassword");
        actions.typeValueInField("password", "registerConfirmPassword");
        actions.waitFor(1000);
        actions.clickElementAfterWait("register.Button");
    }
    public void assertRegisterNewUser() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("updateProfile.Button");
    }
}











