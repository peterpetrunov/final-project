package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;

public class UsersPage extends BasePage {
    public UsersPage() {
        super("users.url");
    }

    public void searchByName(String name) {
        actions.waitForElementVisible("search.Field", 20);
        actions.typeValueInField("Peter", "search.Field");
        actions.clickElement("search.Button");
    }
}