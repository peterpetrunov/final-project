package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditProfilePage extends BasePage {

    public EditProfilePage() {
        super("profileEdit.url");
    }

    private final UsersPage usersPage = new UsersPage();
    public void updateProfileByLastName() {
        actions.waitForElementVisible("lastName.Field", 30);
        actions.clearField("lastName.Field");
        actions.typeValueInFieldByConfProperty("updatedProfileLastName", "lastName.Field");
        actions.waitMillis(1000);
        actions.clickElement("save.Button");
        actions.waitMillis(1000);
    }
//    public void editProfilePictureVisibility() {
//        actions.waitForElementVisible("visibility.Picture", 30);
//        actions.clickElement("visibility.Picture");
//        Select selectBox = new Select(driver.findElement(By.id("personalProfile.picturePrivacy")));
//        selectBox.selectByVisibleText("Public");
//        actions.waitMillis(1000);
//        actions.clickElement("saveChanges.Button");
//        actions.waitMillis(1000);
//    }

//    public void assertVisibilitySuccessfullyChanged() {
//        assertPageNavigated();
//        actions.waitForElementVisible("visibility.Picture", 30);
//        actions.assertElementPresent("visibility.Picture");
//        actions.clickElement("saveChanges.Button");
//    }

    public void assertLastNameChanged() {
        usersPage.navigateToPage();
        actions.waitMillis(1000);
        actions.assertTextEquals("updatedProfileLastName");
    }
}

