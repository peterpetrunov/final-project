package testCases.PrivatePart;

import com.telerikacademy.finalproject.pages.EditProfilePage;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import testCases.BaseTest;

public class ProfileUpdateTests extends BaseTest {

    private static final EditProfilePage editProfilePage = new EditProfilePage();
    private static final LogInPage logInPage = new LogInPage();


    @BeforeClass
    public static void testClassInit() {
        logInPage.navigateToPage();
        logInPage.logIn(UserActions.getTextWithKey("username"),
                UserActions.getTextWithKey("password"));
    }
    @Before
    public void preconditions() {
        editProfilePage.navigateToPage();
    }
    @Test
    public void TC007_ChangeName() {
        editProfilePage.updateProfileByLastName();
        actions.waitMillis(1000);
        editProfilePage.assertLastNameChanged();
    }
}

