package testCases.PrivatePart;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.LogOutPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import testCases.BaseTest;


public class LogInLogOutTests extends BaseTest {
    private static final String invalidPassword = "123";
    private static final String errorMessage = "Wrong username or password.";
    private final NavigationPage navigationPage = new NavigationPage();
    private final LogInPage logInPage = new LogInPage();
    private final LogOutPage logOutPage = new LogOutPage();

    @Before
    public void preconditions() {
        logInPage.navigateToPage();
    }

    @Test
    public void TC13_UnsuccessfulLogInWithInvalidPassword() {
        logInPage.logIn(UserActions.getTextWithKey("username"),
                UserActions.getTextWithKey(invalidPassword));
        logInPage.assertWrongPasswordMessage(errorMessage);
        actions.waitMillis(1000);
    }

    @Test
    public void TC006_SuccessfulLogIn() {
        logInPage.logIn(UserActions.getTextWithKey("username"),
                UserActions.getTextWithKey("password"));
        navigationPage.assertPageNavigated();
        actions.waitMillis(1000);

    }

    @Test
    public void SuccessfulLogOut() {
        logInPage.logIn(UserActions.getTextWithKey("username"),
                UserActions.getTextWithKey("password"));
        logOutPage.logOut();
        navigationPage.assertPageNavigated();
        actions.waitMillis(1000);

    }
}