package testCases.PrivatePart;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.LogOutPage;
import com.telerikacademy.finalproject.pages.PostsPage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.runners.MethodSorters;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.FixMethodOrder;
import testCases.BaseTest;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)


public class NewPostTests extends BaseTest {
    private static final PostsPage postsPage = new PostsPage();
    private static final LogInPage logInPage = new LogInPage();
    private static final LogOutPage logOutPage = new LogOutPage();


    @BeforeClass
    public static void testClassInit() {
        logInPage.navigateToPage();
        logInPage.logIn(UserActions.getTextWithKey("username"),
                UserActions.getTextWithKey("password"));
    }

    @Before
    public void testInit() {
        postsPage.navigateToPage();
    }

    @AfterClass
    public static void testClassTearDown() {
        logOutPage.navigateToPage();
        logOutPage.logOut();
    }


    @Test
    public void TC017_CreatePrivatePost() {
        postsPage.createPrivatePost();
        postsPage.assertPrivatePostExists();
    }

    @Test
    public void TC016_CreatePublicPost() {
        postsPage.createPublicPost();
        postsPage.assertPublicPostExists();
    }

    @Test
    public void TC018_EditPost() {
        postsPage.editPost();
        postsPage.assertEditPost();
    }

    @Test
    public void TC020_LikePost() {
        postsPage.likePost();
        postsPage.assertLikePost();
    }

    @Test
    public void TC021_DislikePost() {
        postsPage.dislikePost();
        postsPage.assertDislikePost();
    }

    @Test
    public void TC022_CommentPost() {
        postsPage.commentPost();
        postsPage.assertCommentPost();
    }

    @Test
    public void TC019_DeletePost() {
        postsPage.deletePost();
        postsPage.assertDeletePost();
    }
}

