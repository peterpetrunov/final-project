package testCases.PublicPart;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import testCases.BaseTest;

    public class SearchUsersTests extends BaseTest {
    private static final UsersPage usersPage = new UsersPage();
    private static final NavigationPage navigationPage = new NavigationPage();

    @Before
    public void preconditions() {
        navigationPage.navigateToPage();
    }

    @AfterClass
    public static void testClassTearDown() {
            UserActions.quitDriver();
    }

    @Test
    public void TC002_SearchForUsers() {
        usersPage.searchByName("searched.Name");
        actions.waitMillis(1000);
        usersPage.actions.assertTextEquals("updatedProfileLastName");

    }
}
