package testCases.AdminPart;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.*;
import testCases.BaseTest;

public class DeletePostTest extends BaseTest {
    private static final LogInPage logInPage = new LogInPage();
    private static final LogOutPage logOutPage = new LogOutPage();
    private final DeletePostPage deletePostsPage = new DeletePostPage();


    @BeforeClass
    public static void testClassInit() {
        logInPage.navigateToPage();
        logInPage.logIn(UserActions.getTextWithKey("usernameAdmin"),
                UserActions.getTextWithKey("passwordAdmin"));
    }

    @Before
    public void testInit() {
        deletePostsPage.navigateToPage();
    }

    @AfterClass
    public static void testClassTearDown() {
        logOutPage.logOut();
    }

    @Test
    public void TC35_DeletePost() {
        deletePostsPage.deletePost();
        deletePostsPage.assertDeletePost();
    }
}
