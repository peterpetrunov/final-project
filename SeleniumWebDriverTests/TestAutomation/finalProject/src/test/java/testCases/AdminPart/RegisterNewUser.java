package testCases.AdminPart;

import com.telerikacademy.finalproject.pages.*;
import org.junit.Before;
import org.junit.Test;
import testCases.BaseTest;

public class RegisterNewUser extends BaseTest {
    private final RegisterPage registerPage = new RegisterPage();


    @Before
    public void testInit() {
        registerPage.navigateToPage();
    }


    @Test
    public void TC001_RegisterNewUser() {
        registerPage.registerNewUser();
        registerPage.assertRegisterNewUser();
    }
}